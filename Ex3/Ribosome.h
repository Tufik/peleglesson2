#pragma once

#define NUCLEOTIDE_LENGTH 3

#include "Protein.h"

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;

};

