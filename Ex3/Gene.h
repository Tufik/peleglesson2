#pragma once

class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	void set_start(const unsigned int start);
	void set_end(const unsigned int end);
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
};
