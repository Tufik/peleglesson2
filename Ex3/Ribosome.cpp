#include "Ribosome.h"

/*
	Method returns the protein that matches the rna transcript.

	Input:
			RNA_transcript - The rna transcript.

	Output:
			The apropriate protein.
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* retProtein = new Protein;
	std::string nucleotide = "";
	unsigned int i = 0;
	
	retProtein->init();

	for (i = 0; i < RNA_transcript.length(); i += NUCLEOTIDE_LENGTH)
	{
		nucleotide = RNA_transcript.substr(i, NUCLEOTIDE_LENGTH);
		retProtein->add(get_amino_acid(nucleotide));
	}

	return retProtein;
}