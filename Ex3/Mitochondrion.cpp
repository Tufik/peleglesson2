#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
	Method checks if the protein is of a certain form and updates the '_has_glocuse_receptor' attribute.

	Input:
			protein - The protein to check.

	Output:
			void.
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* acidNode = protein.get_first();
	AminoAcid arr[RECEPTOR_LENGTH] = { AminoAcid::ALANINE, AminoAcid::LEUCINE, AminoAcid::GLYCINE, AminoAcid::HISTIDINE, AminoAcid::LEUCINE, AminoAcid::PHENYLALANINE, AminoAcid::AMINO_CHAIN_END };

	this->_has_glocuse_receptor = true;	//Needed because of the '&&' operation.

	for (unsigned int i = 0; i < RECEPTOR_LENGTH; i++)
	{
		this->_has_glocuse_receptor = this->_has_glocuse_receptor && acidNode->get_data() == arr[i];
		acidNode = acidNode->get_next();
	}
}


void Mitochondrion::set_glocuse(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
	Method checks if the mito can produce atp.

	Input:
			void.

	Output:
			true if the mito can create atp.
*/
bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glocuse_level > MIN_GLOCUSE_LEVEL;
}