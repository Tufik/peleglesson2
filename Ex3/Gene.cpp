#include "Gene.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


unsigned int Gene::get_start() const
{
	return this->_start;
}


unsigned int Gene::get_end() const
{
	return this->_end;
}


bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}


void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}


void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}


void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}