#pragma once

#include "Protein.h"

#define RECEPTOR_LENGTH 7
#define MIN_GLOCUSE_LEVEL 50

class Mitochondrion
{
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;

public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glocuse(const unsigned int glocuse_units);
	bool produceATP() const;
};

