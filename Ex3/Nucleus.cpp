#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


unsigned int Gene::getStart() const
{
	return this->_start;
}


unsigned int Gene::getEnd() const
{
	return this->_end;
}


bool Gene::getOnComplementaryDnaStrand() const
{
	return this->_on_complementary_dna_strand;
}


void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}


void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}


void Gene::setOnComplementaryDnaStrand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	for (unsigned int i = 0; i < dna_sequence.length(); i++)
	{
		switch (dna_sequence[i])
		{
			case 'A':
			{
				this->_DNA_strand += 'A';
				this->_complementary_DNA_strand += 'T';
				break;
			}

			case 'T':
			{
				this->_DNA_strand += 'T';
				this->_complementary_DNA_strand += 'A';
				break;
			}

			case 'C':
			{
				this->_DNA_strand += 'C';
				this->_complementary_DNA_strand += 'G';
				break;
			}

			case 'G':
			{
				this->_DNA_strand += 'G';
				this->_complementary_DNA_strand += 'C';
				break;
			}

			default:
			{
				std::cerr << "Invalid dna sequence" << std::endl;
				_exit(1);
				break;
			}
		}
	}
}

/*
	Method returns the rna transcript of the gene.

	Input:
			gene - The gene to get the rna from.

	Output:
			The string containing the rna.
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_transcript = "";
	
	for (unsigned int i = gene.getStart(); i <= gene.getEnd(); i++)
	{
		if (!gene.getOnComplementaryDnaStrand())
		{
			if (this->_DNA_strand[i] == 'T')
			{
				RNA_transcript += 'U';
			}

			else
			{
				RNA_transcript += this->_DNA_strand[i];
			}
		}
		else
		{
			if (this->_complementary_DNA_strand[i] == 'T')
			{
				RNA_transcript += 'U';
			}

			else
			{
				RNA_transcript += this->_complementary_DNA_strand[i];
			}
		}
	}

	return RNA_transcript;
}


/*
	Method returns the reversed dna strand.

	Input:
			void.

	Output:
			The dna strand in reverse order.
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string rev = "";

	for (int i = this->_DNA_strand.length(); i > 0; i--)
	{
		rev += this->_DNA_strand[i - 1];
	}

	return rev;
}

/*
	Method returns the number of appearences of a codon in a dna strand.

	Input:
			codon - The string of the codon.

	Output:
			The number of times that the codon appears in the dna strand.
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int count = 0;
	bool isInStrand = true;
	
	for (i = 0; i < this->_DNA_strand.length() - CODON_LENGTH; i++)
	{
		for (j = 0; j < CODON_LENGTH && isInStrand; j++)
		{
			if (this->_DNA_strand[i + j] != codon[j])
			{
				isInStrand = false;
			}
		}

		if (isInStrand)
		{
			count++;
		}
		isInStrand = false;
	}


	return count;
}
