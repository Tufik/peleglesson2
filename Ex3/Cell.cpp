#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

/*
	Method gets atp.

	Input:
			void.

	Output:
			if atp creation succeeded, return true.
*/
bool Cell::get_ATP()
{
	this->_mitochondrion.insert_glucose_receptor(*this->_ribosome.create_protein(this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene)));
	
	if (!this->_mitochondrion.produceATP())
	{
		return false;
	}

	this->_atp_units = 100;

	return true;
}

